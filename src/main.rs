mod models;
mod tcp;
mod udp;

use config::Config;
use dotenv::dotenv;
use env_logger::Env;
use log::info;
use tokio::signal;

// Note for the future :
//
// pub type Clients = Arc<Mutex<HashMap<String, Client>>>;
// Global var

#[tokio::main]
async fn main() {
    dotenv().ok();
    // Init env logger
    env_logger::Builder::from_env(Env::default()).init();

    info!("Starting UwUProxy Proxy...");

    // Search for config file
    let settings = Config::builder()
        .add_source(config::File::with_name("config.toml"))
        .build()
        .unwrap();
    // Unwrap and deserialize Config file
    let config = settings.try_deserialize::<models::AppConfig>().unwrap();
    info!("{:?}", config);

    // Init TCP proxy
    tcp::init(config.tcp.unwrap()).await;
    udp::init(config.udp.unwrap()).await;

    match signal::ctrl_c().await {
        Ok(()) => {}
        Err(err) => {
            eprintln!("Unable to listen for shutdown signal: {}", err);
            // we also shut down in case of error
        }
    }
}
