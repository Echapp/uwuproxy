use log::info;
use std::collections::HashMap;
use tokio::net::{TcpListener, TcpStream};
use tokio::try_join;

// HIGHLY inspired by : https://anirudhsingh.dev/blog/2021/07/writing-a-tcp-proxy-in-rust/

pub async fn init(list: HashMap<String, crate::models::Server>) {
    for (_name, server) in list {
        tokio::spawn(async move {
            let proxy_server = TcpListener::bind(server.clone().from)
                .await
                .unwrap_or_else(|e| {
                    panic!("Failed to bind: {}", e);
                });
            info!(
                "TCP Proxy started. from {} to {}",
                server.clone().from,
                server.clone().to
            );

            while let Ok((client, _)) = proxy_server.accept().await {
                let server_infos = server.clone();
                tokio::spawn(async move {
                    info!("New client. Redirected to {}", server_infos.clone().to);
                    handle_client_conn(client, server_infos.clone().to)
                        .await
                        .expect("TODO: panic message");
                });
            }
        });
    }
}

async fn handle_client_conn(
    mut client_conn: TcpStream,
    to: String,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut server_conn = TcpStream::connect(to).await?;
    let (mut client_recv, mut client_send) = client_conn.split();
    let (mut server_recv, mut server_send) = server_conn.split();

    let handle_one = async { tokio::io::copy(&mut server_recv, &mut client_send).await };

    let handle_two = async { tokio::io::copy(&mut client_recv, &mut server_send).await };

    try_join!(handle_one, handle_two)?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use tokio::io::{AsyncReadExt, AsyncWriteExt};

    use super::*;

    #[tokio::test]
    async fn tcp_proxy() {
        // Create tcp connexion with tokio
        let proxy_server = TcpListener::bind("0.0.0.0:3086").await.unwrap_or_else(|e| {
            panic!("Failed to bind: {}", e);
        });

        // Initiate tcp proxy
        let mut map = HashMap::new();
        map.insert(
            "test".to_string(),
            crate::models::Server {
                enabled: true,
                from: "0.0.0.0:3085".to_string(),
                to: "0.0.0.0:3086".to_string(),
            },
        );
        crate::tcp::init(map).await;

        // Spawn a task to send data to the proxy
        tokio::spawn(async move {
            // Wait 1 seconds
            tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;

            // Connect to proxy
            let mut connexion_test = TcpStream::connect("0.0.0.0:3085")
                .await
                .unwrap_or_else(|e| {
                    panic!("Failed to connect: {}", e);
                });

            // Send data to proxy
            connexion_test
                .write_all(b"Hello, world!")
                .await
                .unwrap_or_else(|e| {
                    panic!("Failed to write: {}", e);
                });
        });

        let mut buf = [0; 1024];
        // Get first connexion
        let (mut client_conn, _) = proxy_server.accept().await.unwrap_or_else(|e| {
            panic!("Failed to accept: {}", e);
        });
        // Read data from connexion
        let n = client_conn.read(&mut buf).await.unwrap_or_else(|e| {
            panic!("Failed to read: {}", e);
        });

        assert_eq!(&buf[..n], b"Hello, world!")
    }
    // TODO: TCP Proxy with multiple clients
}
