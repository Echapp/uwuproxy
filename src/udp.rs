use log::info;
use std::{collections::HashMap, net::SocketAddr};
use tokio::net::UdpSocket;

pub async fn init(list: HashMap<String, crate::models::Server>) {
    for (_name, server) in list {
        let join_handle = tokio::spawn(async move {
            let proxy_server = UdpSocket::bind(server.clone().from).await.unwrap();
            let proxy_addr = proxy_server.local_addr().unwrap();
            info!(
                "UDP Proxy started. from {} to {}",
                server.clone().from,
                server.clone().to
            );

            let mut buf = [0; 65535];
            loop {
                match proxy_server.recv_from(&mut buf).await {
                    Ok((n, src)) => {
                        let data = &buf[..n];
                        let server_addr: SocketAddr = server.clone().to.parse().unwrap();

                        // Forward the received packet to the target server
                        proxy_server.send_to(data, server_addr).await.unwrap();

                        info!("New client. Redirected to {}", server.clone().to);
                    }
                    Err(e) => {
                        eprintln!("error receiving datagram: {}", e);
                    }
                }
            }
        });
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn udp_proxy() {
        // Create UDP Connexion with tokio
        let proxy_server = UdpSocket::bind("0.0.0.0:3086").await.unwrap_or_else(|e| {
            panic!("Failed to bind: {}", e);
        });

        // Initiate UDP proxy
        let mut map = HashMap::new();
        map.insert(
            "test".to_string(),
            crate::models::Server {
                enabled: true,
                from: "0.0.0.0:3085".to_string(),
                to: "0.0.0.0:3086".to_string(),
            },
        );
        crate::udp::init(map).await;

        // Spawn a task to send data to the proxy
        tokio::spawn(async move {
            // Wait 1s
            tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;

            let client_conn = UdpSocket::bind("0.0.0.0:3087").await.unwrap_or_else(|e| {
                panic!("Failed to bind: {}", e);
            });

            // Send data to proxy
            client_conn
                .send_to(b"Hello, world!", "0.0.0.0:3085")
                .await
                .unwrap_or_else(|e| {
                    panic!("Failed to write: {}", e);
                });
        });

        // Receive data from proxy
        let mut buf = [0u8; 1024];
        let (size, _addr) = proxy_server.recv_from(&mut buf).await.unwrap_or_else(|e| {
            panic!("Failed to read: {}", e);
        });
        // Check if data is correct
        assert_eq!(&buf[..size], b"Hello, world!");
    }
}
