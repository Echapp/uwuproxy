use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Server {
    pub enabled: bool,
    pub from: String,
    pub to: String,
}
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct AppConfig {
    pub tcp: Option<HashMap<String, Server>>,
    pub udp: Option<HashMap<String, Server>>,
}
