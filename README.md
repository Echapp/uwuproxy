# uwuproxy ( WORK IN PROGRESS, NOT USEABLE YET )

Very simple TCP Proxy for Echapp. ( WORK IN PROGRESS, NOT USEABLE YET )

## Simple explanation
![Server A have two services, he is connected to Server B with a VPN, server be redirect with uwu proxy to each services.](/assets/images/whatwhy.svg)

## Goal

The goal is to create a simple TCP AND UDP proxy that can be used to redirect traffic to a specific port on different servers. This is useful for example if you have a server that is connected to a VPN and you want to redirect traffic to a specific port to a specific server. I want, in the future, to add a simple API to manage the proxy, so I want something dynamic.

> I want to use it in production, so if you have optimiations ideas, please open an issue or a pull request !

## Todo

Before PROD
- [ ] Useable
- [x] Add support for UDP
- [ ] Rate limit
- [ ] Cache

After that
- [ ] API
- [ ] File based Database ( PickleDb ? )
- [ ] Logging ( Grafana ? )
- [ ] Optimizations ( For production !!! )
In the future...
- [ ] UDP to TCP proxy
- [ ] TCP to UDP proxy